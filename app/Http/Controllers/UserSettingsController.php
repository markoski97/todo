<?php

namespace App\Http\Controllers;

use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserSettingsController extends Controller
{
    public function index(){
        return view('user_settings');
    }

    public function updateProfile(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);

        $user=User::findorfail(Auth::id());


        $user->name=$request->name;
        $user->email=$request->email;
        $user->save();

        Toastr::success('User Updated', 'Success');
        return redirect()->back();
    }

    public function updatePassword(Request $request){
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed',
        ]);

        $hashPassword=Auth::user()->password;

        if(Hash::check($request->old_password,$hashPassword)){
            if(!Hash::check($request->password,$hashPassword)){
                $user=User::find(Auth::id());
                $user->password=Hash::make($request->password);
                $user->save();
                Toastr::success('Password changed', 'Success');
                Auth::logout();
                return redirect()->back();
            }
            else{
                Toastr::error('Password cannot be the same as the old one', 'Error');
                return redirect()->back();
            }
        }else{
            Toastr::error('Passwords must match', 'Error');
            return redirect()->back();
        }
    }
}
