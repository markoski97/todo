<?php

namespace App\Http\Controllers;

use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {

        $tasksTodo = Auth::user()->tasks()->where('category_id', '=', 1)->count();
        $tasksInProgress = Auth::user()->tasks()->where('category_id', '=', 2)->count();
        $tasksDone = Auth::user()->tasks()->where('category_id', '=', 3)->count();
        $tasksAll = Auth::user()->tasks()->count();
        $tasksDue = Auth::user()->tasks()->whereDate('deadline', '<=', Carbon::today()->addDays(2))->get();

        $oldPost = Auth::user()->tasks()->where('category_id', '=', '3')
            ->where('updated_at', '>=', now()->addDays(2))->select('name', 'label', 'created_at', 'updated_at')
            ->each(function ($oldRecord) {
                $newPost = $oldRecord->replicate();
                $newPost->completed_at = $oldRecord->updated_at;
                $newPost->task_created_at = $oldRecord->created_at;
                $newPost->user_id = Auth()->id();
                $newPost->setTable('task_archives');
                $newPost->save();
            });

        $oldPostDelete = Auth::user()->tasks()->where('category_id', '=', '3')
            ->where('updated_at', '>=', now()->addDays(2))->delete();


        return view('dashboard', compact('tasksDone', 'tasksInProgress', 'tasksTodo', 'tasksAll', 'tasksDue'));
    }
}
