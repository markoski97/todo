<?php

namespace App\Http\Controllers;

use App\Task;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class TaskController extends Controller
{

    public function index()
    {
        $tasksTodo=Auth::user()->tasks()->where('category_id' ,'=', 1)->get();

        return view('tasks.todo',compact('tasksTodo'));
    }

    public function create()
    {
       return view('tasks.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $slug=Str::slug($request->name);

        $task=new Task();

        $task->name=$request->name;
        $task->label=$request->label;
        $task->deadline=Carbon::parse($request->deadline);

        $task->user_id=Auth()->id();
        $task->category_id=1;//meaning to do
        $task->slug=$slug;

        $task->save();
        Toastr::success('Post Created', 'Success');

        return redirect()->route('task.index');

    }

    public function edit(Task $task)
    {
        if($task->user_id  !== Auth::id())
        {
            Toastr::error('You have no autorization','error');
            return redirect()->back();
        }
        return view('tasks.edit',compact('task'));
    }

    public function update(Request $request, Task $task)
    {
        if($task->user_id  !== Auth::id())
        {
            Toastr::error('You have no autorization','error');
            return redirect()->back();
        }

        $this->validate($request, [
            'name' => 'required',
        ]);

        $slug=Str::slug($request->name);

        $task->name=$request->name;
        $task->label=$request->label;
        $task->deadline=Carbon::parse($request->deadline);

        $task->user_id=Auth()->id();
        $task->category_id=1;//meaning to do
        $task->slug=$slug;

        $task->save();
        Toastr::success('Task Updated Created', 'Success');

        return redirect()->back();
    }

    public function destroy(Task $task)
    {
        if($task->user_id  !== Auth::id())
        {
            Toastr::error('You have no autorization','error');
            return redirect()->back();
        }

        $task->delete();
        Toastr::success('Post Deleted', 'Success');

        return redirect()->back();
    }

    public function moveToInProgress(Task $task)
    {
        if($task->user_id  !== Auth::id())
        {
            Toastr::error('You have no autorization','error');
            return redirect()->back();
        }
        $task->category_id=2;

        $task->save();

        Toastr::success('Task moved to In Progress', 'Success');
        return redirect()->back();
    }

    public function moveToDone(Task $task)
    {
        if($task->user_id  !== Auth::id())
        {
            Toastr::error('You have no autorization','error');
            return redirect()->back();
        }

        $task->category_id=3;

        $task->save();

        Toastr::success('Task moved to In Progress', 'Success');
        return redirect()->back();
    }

    public function moveToToDo(Task $task)
    {
        if($task->user_id  !== Auth::id())
        {
            Toastr::error('You have no autorization','error');
            return redirect()->back();
        }

        $task->category_id=1;

        $task->save();

        Toastr::success('Task moved to In Progress', 'Success');
        return redirect()->back();
    }

    public function inProgress(Task $task)
    {
        $tasksInProgress=Auth::user()->tasks()->where('category_id' ,'=', 2)->get();

        return view('tasks.in-progress',compact('tasksInProgress'));
    }

    public function Done(Task $task)
    {
        /*$tasksDone=Task::where('category_id' ,'=', 3)->get();*/
        $tasksDone=Auth::user()->tasks()->where('category_id' ,'=', 3)->get();

        return view('tasks.done',compact('tasksDone'));
    }

    public function Archive()
    {
        $archives=Auth::user()->archiveTasks()->get();

        return view('archive.index',compact('archives'));
    }


}
