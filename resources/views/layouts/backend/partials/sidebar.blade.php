<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            {{-- <img src="{{Storage::disk('public')->url('profile/'.Auth::user()->image)}}" width="48" height="48" alt="User"/>--}}
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true"
                 aria-expanded="false">{{Auth::user()->name}}</div>
            <div class="email">{{Auth::user()->email}}</div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">

            <li class="header">ACTIONS</li>

            <li class="{{Request::is('task/create') ? 'active' : ''}}">
                <a href="{{route('task.create')}}">
                    <i class="material-icons">add_box</i>
                    <span>Add Tasks</span>
                </a>
            </li>

            <li class="header">MAIN NAVIGATION</li>

            <li class="{{Request::is('dashboard') ? 'active' : ''}}">
                <a href="{{route('dashboard.index')}}">
                    <i class="material-icons">dashboard</i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="{{Request::is('task') ? 'active' : ''}}">
                <a href="{{route('task.index')}}">
                    <i class="material-icons">list_alt</i>
                    <span>Tasks Todo</span>
                </a>
            </li>

            <li class="{{Request::is('task-inProgress') ? 'active' : ''}}">
                <a href="{{route('task.inProgress')}}">
                    <i class="material-icons">cached</i>
                    <span>Tasks in Progress</span>
                </a>
            </li>

            <li class="{{Request::is('task-done') ? 'active' : ''}}">
                <a href="{{route('task.done')}}">
                    <i class="material-icons">check_circle_outline</i>
                    <span>Tasks Done</span>
                </a>
            </li>

            <li class="header">System</li>

            <li class="{{Request::is('archive') ? 'active' : ''}}">
                <a href="{{route('archive')}}">
                    <i class="material-icons">archive</i>
                    <span>Archive</span>
                </a>
            </li>

            <li class="{{Request::is('settings') ? 'active' : ''}}">
                <a href="{{route('settings')}}">
                    <i class="material-icons">settings</i>
                    <span>Settings</span>
                </a>
            </li>

            <li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="material-icons">input</i>
                    <span>Sign Out</span>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>

    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; Markoski Petar
        </div>
    </div>
    <!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->
