@extends('layouts.backend.app')

@section('title',"Task")

@push('css')

@endpush

@section('content')
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           Archive Tasks
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Label</th>
                                    <th>Task Completed At:</th>
                                    <th>Task Created At:</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Label</th>
                                    <th>Task Completed At:</th>
                                    <th>Task Created At:</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                @foreach($archives as $key=>$archive)
                                    <tr>
                                        <td>{{$key + 1 }}</td>
                                        <td>{{$archive->name }}</td>
                                        <td>{{$archive->label }}</td>
                                        <td>{{$archive->completed_at }}</td>
                                        <td>{{$archive->task_created_at }}</td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('js')

@endpush
