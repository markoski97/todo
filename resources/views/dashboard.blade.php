@extends('layouts.backend.app')

@section('title',"Dashboard")

@push('css')
    <link href="{{asset('assets/backend/plugins/animate-css/animate.css')}}"
          rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h1>DASHBOARD</h1>
        </div>
        <div class="row clearfix">

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-red hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">list_alt</i>
                    </div>
                    <div class="content">
                        <div class="text">ToDo Tasks</div>
                        <div class="number count-to" data-from="0" data-to="{{$tasksTodo}}" data-speed="1000"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">cached</i>
                    </div>
                    <div class="content">
                        <div class="text">In Progress Tasks</div>
                        <div class="number count-to" data-from="0" data-to="{{$tasksInProgress}}" data-speed="1000"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">check_circle_outline</i>
                    </div>
                    <div class="content">
                        <div class="text">Done Tasks</div>
                        <div class="number count-to" data-from="0" data-to="{{$tasksDone}}" data-speed="1000"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-blue hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">Total Tasks</div>
                        <div class="number count-to" data-from="0" data-to="{{$tasksAll}}" data-speed="1000"
                             data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>


        </div>

        @if($tasksDue->count())
        <div class="block-header">
            <h1>Tasks 2days before deadline</h1>
        </div>
        @endif

        <!-- Basic Example -->
        <div class="row clearfix">

            @forelse($tasksDue as $task)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>
                                {{$task->name}}
                                <small>
                                    Due Date: {{\Carbon\Carbon::parse($task->deadline)->diffForHumans()}}
                                </small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="{{route('task.edit',$task->id)}}">
                                                Edit Task
                                            </a>
                                        </li>

                                        <li>
                                            <form action="{{ route('task.moveToInProgress',$task->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                                <button type="submit" style="width: 160px "
                                                        class="btn btn-default waves-effect">Move to In
                                                    Progress
                                                </button>
                                            </form>
                                        </li>

                                        <li>
                                            <form action="{{ route('task.moveToDone',$task->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                                <button style="width: 160px " type="submit"
                                                        class="btn btn-success waves-effect">Move to Done
                                                </button>
                                            </form>
                                        </li>

                                        <li class="btn btn-danger waves-effect" onclick="deleteCategory({{$task->id}})">
                                            Delete Task
                                            <form id="delete-form-{{$task->id}}"
                                                  action="{{route('task.destroy',$task->id)}}" method="Post"
                                                  style="display: none">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            {{$task->label}}
                        </div>
                    </div>
                </div>
            @empty

            @endforelse
        </div>
    </div>
    </div>
@endsection

@push('js')
    <script src="{{asset('assets/backend/js/pages/index.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-countto/jquery.countTo.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.5.1/dist/sweetalert2.all.min.js"></script>
    {{--Za deletirajne na tag--}}
    <script>
        function deleteCategory(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-' + id).submit();
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your task is save',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
