@extends('layouts.backend.app')

@section('title',"Task")

@push('css')
    <!-- Animation Css -->

    <link href="{{asset('assets/backend/plugins/animate-css/animate.css')}}"
          rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h1>In Progress Tasks</h1>
        </div>
        <!-- Basic Example -->
        <div class="row clearfix">

            @forelse($tasksInProgress as $task)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header bg-orange">
                            <h2>
                                {{$task->name}}
                                <small>
                                    Due Date: {{\Carbon\Carbon::parse($task->deadline)->diffForHumans()}}
                                </small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="{{route('task.edit',$task->id)}}">
                                                Edit Task
                                            </a>
                                        </li>

                                        <li>
                                            <form action="{{ route('task.moveToToDo',$task->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                                <button type="submit" style="width: 160px "
                                                        class="btn btn-default waves-effect">Move to ToDo
                                                </button>
                                            </form>
                                        </li>

                                        <li>
                                            <form action="{{ route('task.moveToDone',$task->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                                <button style="width: 160px " type="submit"
                                                        class="btn btn-success waves-effect">Move to Done
                                                </button>
                                            </form>
                                        </li>

                                        <li class="btn btn-danger waves-effect" onclick="deleteCategory({{$task->id}})">
                                            Delete Task
                                            <form id="delete-form-{{$task->id}}"
                                                  action="{{route('task.destroy',$task->id)}}" method="Post"
                                                  style="display: none">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            {{$task->label}}
                        </div>
                    </div>
                </div>
            @empty
                <div>
                    <h3>No tasks in Progress</h3>
                </div>
            @endforelse
        </div>
    </div>

@endsection
@push('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.5.1/dist/sweetalert2.all.min.js"></script>
    {{--Za deletirajne na tag--}}
    <script>
        function deleteCategory(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-' + id).submit();
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your task is save',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
