@extends('layouts.backend.app')

@section('title',"Task")

@push('css')

@endpush

@section('content')


<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Edit Task
                    </h2>
                </div>
                <div class="body">
                    <form action="{{route('task.update',$task->id)}}" method="Post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="name" class="form-control" name="name" value="{{$task->name}}" placeholder="">
                                <label class="form-label">Task Name</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="label" class="form-control" name="label" value="{{$task->label}}" placeholder="">
                                <label class="form-label"> Task Label</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <h2 class="card-inside-title">Task Deadline</h2>
                            <div class="form-line">
                                <input type="datetime-local" id="deadline" class="form-control" name="deadline" value="{{old('time')?? date('Y-m-d\TH:i', strtotime($task->deadline)) }}" placeholder="">
                            </div>
                        </div>

                        <a class="btn btn-danger m-t-15 waves-effect"
                           href="{{route('task.index')}}">
                            Back
                        </a>

                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
@endsection
@push('js')

@endpush

