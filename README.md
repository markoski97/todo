

## Todo App

Backend Developer - Assessment task
Create a fully functional TODO list

Project and git setup:

1. Create a git project.
2. Setup Laravel project and push it as initial commit to newly created git.

Assessment features:

1. As a user I want to have a nice dashboard of 3 columns:
a. TODO – this column is holding all the tasks that I have yet to complete.
b. IN PROGRESS – this column is holding all the tasks I am currently working on.
c. DONE – this column is holding all the tasks I have already completed.

2. As a user I want to be have a quick form with 2 text inputs with which I will be able to create
or edit my tasks (there is also one additional field for additional features this assessment is
giving you):
a. NAME – this field is presenting the name of the task and is the one displayed in a
card on a dashboard.
b. LABEL – this field is presenting the “tag” of the task. Ex.: shopping, work related etc.
c. (additional) DEADLINE – this field is optional and presents when the task should be
done with date and time specified.

3. As a user I want to be able to deleted tasks as a may have made some mistake. For this
action the confirmation popup is required!

4. As a user I want to update my tasks fileds by clicking on it and edit the data in form (could be
the same one used for creating).

5. As a user I want to move my tasks between columns and by that I would like to update the
task status.

6. Now we have the basic functionality covered, next step would be to enable authentication
of different users so they can access their dashboard and work with basic functionality. To
do that we need Register and Login features.

7. As a user I want to be able to create myself an account with basic data:
a. FULL NAME
b. EMAIL
c. PASSWORD

8. As a user I want to login with my email and password I passed in registration form and
access my “TODO list”

9. As a user I want to logout from my “TODO list” when I finish the work.

Congratulations!
To this part the assessment application is finished. You could wrap it up here and send as a link to
your git repo or if you would like and if you have time left you can implement some additional
functionalities.

1. As a user I want to have visual presentation when my task is 2 days befor deadline (it can be
calculated everytime I login my dashboard and on page refresh)

2. As a user I want my done task to be pushed to archive after 2 days in column (it can also be
checked everytime I login or refresh the page)

3. As a user I want to have a special tab in my dashboard where I can switch between
dashboard and table with all the archived tasks. Table columns:

a. Name
b. Label
c. Created at (date + time)
d. Completed at (date + time)

4. As a user I want to be able to change my password and update my profile name.

As backend you will NOT do any design work, so this project shouldn't be nice looking but it should
work perfectly. Main focus is that all main functionalities are working and that the code structure,
file structure etc. is not messy. The deadline for this assessment is 7-days and all work should be
pushed to git so we can check it later.

##Install
- **Clone the repository with git clone**
- **Edit the .env  file whith you credentials**
- **Run composer install**
- **Run php artisan key:generate**
- **Run php artisan migrate --seed**
