<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();
Route::group(['middleware' =>['auth']], function()
{
    Route::get('/dashboard','DashboardController@index')->name('dashboard.index');
    Route::resource('/task','TaskController')->except('show');
    Route::get('/task-inProgress','TaskController@inProgress')->name('task.inProgress');
    Route::get('/task-done','TaskController@Done')->name('task.done');

    Route::put('/task/moveToInProgress/{task}','TaskController@moveToInProgress')->name('task.moveToInProgress');
    Route::put('/task/moveToDone/{task}','TaskController@moveToDone')->name('task.moveToDone');
    Route::put('/task/moveToToDo/{task}','TaskController@moveToToDo')->name('task.moveToToDo');

    Route::get('/archive','TaskController@Archive')->name('archive');

    Route::get('/settings','UserSettingsController@index')->name('settings');
    Route::put('/profile-update','UserSettingsController@updateProfile')->name('profile.update');
    Route::put('/password-update','UserSettingsController@updatePassword')->name('password.update');
});


Route::get('/home', 'HomeController@index')->name('home');


